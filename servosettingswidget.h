#ifndef SERVOSETTINGSWIDGET_H
#define SERVOSETTINGSWIDGET_H

#include <QWidget>
#include "device.h"

namespace Ui {
class ServoSettingsWidget;
}

class ServoSettingsWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ServoSettingsWidget(QWidget *parent = 0);
    ~ServoSettingsWidget();

    void init(Device * dev,int si);
    void save();
private slots:
    void setBusy(bool busy);
private:
    Ui::ServoSettingsWidget *ui;

    Device * _dev;
    int _si;
};

#endif // SERVOSETTINGSWIDGET_H
