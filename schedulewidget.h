#ifndef SCHEDULEWIDGET_H
#define SCHEDULEWIDGET_H

#include <QWidget>
#include "device.h"

namespace Ui {
class ScheduleWidget;
}

class ScheduleWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ScheduleWidget(QWidget *parent = 0);
    ~ScheduleWidget();

    void init(Device * dev, Device::Job job);
    Device::Job getJob();
signals:
    void dropMe();
private slots:
    void on_pushButton_clicked();

private:
    Ui::ScheduleWidget *ui;

    Device * _dev;
    Device::Job _job;
};

#endif // SCHEDULEWIDGET_H
