#ifndef DEVICE_H
#define DEVICE_H

#include <QObject>
#include <QMap>
#include <QList>
#include <QNetworkAccessManager>
#include <QTimer>
#include <QDateTime>

//#include "deviceservo.h"


class Device : public QObject
{
    Q_OBJECT
public:
    explicit Device(QObject *parent = nullptr);

    void initByIDIP(QString id,QString ip);
    void initFromFile(QString id);

    enum State {
        Offline = 0,
        Initializing,
        Ready,
        Busy
    };

    struct ServoConfig {
        int num;
        bool enabled;
        int clickAngle;
        int clickTime;
        int min;
        int max;
    };


    struct Config {
        QString devID;
        QString name;
        QString wifiEssid;
        QString wifiPassword;
        bool wifiIsAP;
        QString masterIP;
        int masterPort;
        bool masterLock;
        QList<Device::ServoConfig> servos;
    };

    struct Job {
        QString name;
        QString cron;
        int servo;
        int angle;
        int time;
    };

    QString title();
    QString IP();
    Device::State state();
    Device::Config config();
    QList<Device::Job> jobs();

    void saveDevIDJobs(QList<Device::Job> jobslist);
    void save();
    void load(QString deviceId);
    void loadSchedules(QString filePath);
    void saveSchedules(QList<Device::Job> jobslist, QString filePath);
signals:
    void stateChanged(Device::State);
    void busyChange(bool);
    void updated();
    void startJob(Device::Job);
public slots:
    void gotPing(QString ip);

    void update();

    void saveMainConfig(Device::Config cfg);
    void saveServosConfig();
    void setServoConfig(int si, Device::ServoConfig cfg);
    void servoGoTo(int sid, int angle, int time=0);

private slots:
//    void replyReadyReadCfg();
    void replyCfgFinished();
    void replyServoCfgFinished();
    void replyGoToFinished();
    void replyMainConfigFinished();

    void on1secTimer();
private:
    bool checkJob(Device::Job j);
    bool checkCronNumber(int n, QString cron);
    void doJob(Device::Job j);
private:
    Device::State _state;

    QString _ip;
    Device::Config _cfg;
    QList<Job> _jobs;

//    QMap<int,DeviceServo*> _servos;

    QNetworkAccessManager qna;
    QTimer t1s;
    QDateTime lastPing;
    qint64 lastSPing;
};

#endif // DEVICE_H
