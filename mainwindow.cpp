#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "deviceview.h"
#include "settingsdialog.h"
#include <QTabWidget>
#include <QMessageBox>
#include <QIcon>

#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setWindowTitle("Takatan WiFi Servos Controller");
    _tray = new QSystemTrayIcon(QIcon(":/icons/res/takatanicon.png"),this);
    _tray->show();

    _core = new Core(this);
    connect(_core,SIGNAL(newDevice(Device*)),this,SLOT(newCoreDevice(Device*)));
    connect(_core,SIGNAL(notifyDeviceState(QString,QString)),this,SLOT(showNotification(QString,QString)));
    connect(_core,SIGNAL(notifyDeviceJob(QString,QString)),this,SLOT(showNotification(QString,QString)));
    ui->tabDevices->clear();
    _core->loadKnownServos();
//    qDebug() << "listen " << ok;
}

MainWindow::~MainWindow()
{
    delete ui;
    _core->deleteLater();
    _tray->deleteLater();
}

void MainWindow::on_actionQuit_triggered()
{
    exit(0);
}

void MainWindow::log(QString log)
{
    qDebug() << "log " << log;
    ui->statusBar->showMessage(log,4000);
}

void MainWindow::on_actionSettings_triggered()
{
    SettingsDialog * dlg = new SettingsDialog(this);
    connect(dlg,SIGNAL(done(bool)),this,SLOT(settingsDialogDone(bool)));
    dlg->setModal(true);
    dlg->show();
}

void MainWindow::settingsDialogDone(bool updated)
{
    SettingsDialog * dlg = qobject_cast<SettingsDialog*>(sender());
    qDebug() << "settings done " << updated;
    dlg->close();
    dlg->deleteLater();
    if (!updated)
        return;
    _core->settingsUpdated();
}

void MainWindow::newCoreDevice(Device *dev)
{
    DeviceView * view = new DeviceView(this);
    view->setDevice(dev);
    int tabi = ui->tabDevices->addTab(view,dev->title());
    view->setTabI(tabi);
    connect(view,SIGNAL(updateTabTitle(int,QString)),this,SLOT(updateTabTitle(int,QString)));
    //    ui->ltDevices->addWidget(view);
}

void MainWindow::updateTabTitle(int tabI, QString title)
{
    qDebug() << "update tab titile " << tabI << " >> " << title;
    ui->tabDevices->setTabText(tabI,title);
}

void MainWindow::on_actionQt_triggered()
{
    QMessageBox::aboutQt(this);
}

void MainWindow::showNotification(QString title, QString text)
{
    _tray->showMessage(title,text);
}
