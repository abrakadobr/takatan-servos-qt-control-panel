#include "httpserver.h"

#include <QStringList>
#include <QRegExp>
#include <QTcpSocket>
#include <QDateTime>

#include "core.h"

HTTPServer::HTTPServer(Core *core) : QTcpServer(core)
{
    _core = core;
    connect(this,SIGNAL(newConnection()),this,SLOT(newClient()));
}

void HTTPServer::newClient()
{
    emit toLog("new connection");
    QTcpSocket* s = nextPendingConnection();
    connect(s, SIGNAL(readyRead()), this, SLOT(readClient()));
    connect(s, SIGNAL(disconnected()), s, SLOT(deleteLater()));
}

/*
void HTTPServer::incomingConnection(qintptr handle)
{
//    if (started)
//        return;
    emit toLog("new connection");
    QTcpSocket* s = nextPendingConnection();
    connect(s, SIGNAL(readyRead()), this, SLOT(readClient()));
    connect(s, SIGNAL(disconnected()), s, SLOT(deleteLater()));
//    s->setSocketDescriptor(handle);
}
*/

void HTTPServer::readClient()
{
    QTcpSocket* socket = (QTcpSocket*)sender();
    QString req = QString::fromStdString(socket->readAll().toStdString());
    QStringList tokens = req.split(QRegExp("[ \r\n][ \r\n]*"));
    if (tokens[0]=="GET"&&tokens[1].startsWith("/ping"))
    {
        QString did = tokens[1].remove(0,6);
        emit ping(socket->peerAddress().toString(),did);
    }
//    if (tokens[0] == "GET") {
    QTextStream ts(socket);
    ts.setAutoDetectUnicode(true);
    ts << "HTTP/1.0 200 Ok\r\n"
        "Content-Type: text/plain; charset=\"utf-8\"\r\n"
        "\r\n"
        "OK\n"
        << QDateTime::currentDateTime().toString() << "\n";
    socket->close();
//    }
}
