#ifndef SERVOWIDGET_H
#define SERVOWIDGET_H

#include <QWidget>
#include "device.h"

namespace Ui {
class ServoWidget;
}

class ServoWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ServoWidget(QWidget *parent = 0);
    ~ServoWidget();

    void init(Device * dev, int si);
public slots:
    void setBusy(bool busy);
private slots:
//    void on_sbAngle_valueChanged(int arg1);

//    void on_sbTime_valueChanged(int arg1);

//    void on_groupBox_clicked(bool checked);

    void on_pbGoTo_clicked();

    void on_pbZero_clicked();

private:
    Ui::ServoWidget *ui;

    Device * _dev;
    int _si;
};

#endif // SERVOWIDGET_H
