#ifndef CORE_H
#define CORE_H

#include <QObject>
#include <QMap>
#include "httpserver.h"
#include "device.h"

class Core : public QObject
{
    Q_OBJECT
public:
    explicit Core(QObject *parent = nullptr);
    ~Core();

    void loadKnownServos();

    Device * deviceByID(QString did);
    Device * registerDevice(QString id,QString ip);
    Device * loadDevice(QString id);

signals:
    void toLog(QString);
    void newDevice(Device*);

    void notifyDeviceState(QString,QString);
    void notifyDeviceJob(QString,QString);
public slots:
    void settingsUpdated();
    void restartHttpServer();
    void pingFrom(QString ip,QString devid);
private slots:
    void deviceStateUpdated(Device::State state);
    void deviceStartedJob(Device::Job job);
private:
    HTTPServer * httpServer;
    QMap<QString,Device*> devices;
};

#endif // CORE_H
