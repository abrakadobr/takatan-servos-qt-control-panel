#ifndef HTTPSERVER_H
#define HTTPSERVER_H

#include <QTcpServer>

class Core;

class HTTPServer : public QTcpServer
{
    Q_OBJECT
public:
    HTTPServer(Core * core);
protected:
//    void incomingConnection(qintptr handle);
signals:
    void toLog(QString);
    void ping(QString,QString);
private slots:
    void newClient();
    void readClient();
private:
    Core * _core;
};

#endif // HTTPSERVER_H
