#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSettings>
#include <QSystemTrayIcon>

#include "core.h"
#include "device.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_actionQuit_triggered();

    void log(QString log);
    void on_actionSettings_triggered();

    void settingsDialogDone(bool updated);
    void newCoreDevice(Device* dev);
    void updateTabTitle(int tabI, QString title);
    void on_actionQt_triggered();

    void showNotification(QString title, QString text);
private:
    Ui::MainWindow *ui;

    Core * _core;

    QSettings settings;

    QSystemTrayIcon * _tray;
};

#endif // MAINWINDOW_H
