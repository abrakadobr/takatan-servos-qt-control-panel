#include "servowidget.h"
#include "ui_servowidget.h"

ServoWidget::ServoWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ServoWidget)
{
    ui->setupUi(this);
}

ServoWidget::~ServoWidget()
{
    delete ui;
}

void ServoWidget::init(Device *dev, int si)
{
    _dev = dev;
    _si = si;
    connect(_dev,SIGNAL(busyChange(bool)),this,SLOT(setBusy(bool)));
    if (_dev->config().servos.size()<si)
        return;
    Device::ServoConfig cfg = _dev->config().servos.at(si);
    ui->sbAngle->setValue(cfg.clickAngle);
    ui->sbTime->setValue(cfg.clickTime);
    ui->groupBox->setTitle(QString("Servo %1").arg(_si+1));
}

void ServoWidget::setBusy(bool busy)
{
    if (!_dev->config().servos.at(_si).enabled)
        return;
    ui->pbGoTo->setEnabled(!busy);
    ui->sbAngle->setEnabled(!busy);
    ui->sbTime->setEnabled(!busy);
    ui->pbZero->setEnabled(!busy);
}

/*
void ServoWidget::on_sbAngle_valueChanged(int arg1)
{
    Device::ServoConfig cfg = _dev->config().servos.at(_si);
    cfg.clickAngle = arg1;
    _dev->setServoConfig(_si,cfg);
}

void ServoWidget::on_sbTime_valueChanged(int arg1)
{
    Device::ServoConfig cfg = _dev->config().servos.at(_si);
    cfg.clickTime = arg1;
    _dev->setServoConfig(_si,cfg);
}

void ServoWidget::on_groupBox_clicked(bool checked)
{
    Device::ServoConfig cfg = _dev->config().servos.at(_si);
    cfg.enabled = checked;
    _dev->setServoConfig(_si,cfg);
}
*/

void ServoWidget::on_pbGoTo_clicked()
{
    _dev->servoGoTo(_si,ui->sbAngle->value(),ui->sbTime->value());
}

void ServoWidget::on_pbZero_clicked()
{
    _dev->servoGoTo(_si,0,0);
}
