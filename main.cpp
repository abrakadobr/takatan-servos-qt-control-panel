#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QCoreApplication::setOrganizationName("Takatan Software");
    QCoreApplication::setOrganizationDomain("takatan.tk");
    QCoreApplication::setApplicationName("TFingers Controller");

    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}
