#ifndef DEVICEVIEW_H
#define DEVICEVIEW_H

#include <QWidget>
#include <QList>
#include "device.h"

namespace Ui {
class DeviceView;
}

class DeviceView : public QWidget
{
    Q_OBJECT

public:
    explicit DeviceView(QWidget *parent = 0);
    ~DeviceView();

    void setDevice(Device * dev);
    void setTabI(int ti);
signals:
    void updateTabTitle(int,QString);
private slots:
    void deviceStateChanged(Device::State state);
    void reinit();

    void on_pushButton_clicked();

    void setBusy(bool busy);
    void on_pbSaveMainSettings_clicked();

    void on_pbAddSchedule_clicked();

    void on_pbSaveSchedule_clicked();

    void dropJob();
    void on_pbSStoFile_clicked();

private:
    void clearLayout(QLayout * lt);

private:
    Ui::DeviceView *ui;
    int _tabI;

    Device * _dev;
    bool initialized;
    bool servosHidden;

};

#endif // DEVICEVIEW_H
