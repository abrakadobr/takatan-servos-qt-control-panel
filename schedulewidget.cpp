#include "schedulewidget.h"
#include "ui_schedulewidget.h"

ScheduleWidget::ScheduleWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ScheduleWidget)
{
    ui->setupUi(this);
}

ScheduleWidget::~ScheduleWidget()
{
    delete ui;
}

void ScheduleWidget::init(Device *dev, Device::Job job)
{
    _dev = dev;
    _job = job;
    ui->sbServo->setMaximum(1);
    ui->sbServo->setMaximum(dev->config().servos.length());
    ui->sbServo->setValue(job.servo);
    ui->leName->setText(job.name);
    ui->leCron->setText(job.cron);
    ui->sbAngle->setValue(job.angle);
    ui->sbTime->setValue(job.time);
}

Device::Job ScheduleWidget::getJob()
{
    _job.name = ui->leName->text();
    _job.cron = ui->leCron->text();
    _job.servo = ui->sbServo->value();
    _job.angle = ui->sbAngle->value();
    _job.time = ui->sbTime->value();
    return _job;
}

void ScheduleWidget::on_pushButton_clicked()
{
    emit dropMe();
}
