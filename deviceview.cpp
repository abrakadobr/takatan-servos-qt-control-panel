#include "deviceview.h"
#include "ui_deviceview.h"

#include "servowidget.h"
#include "servosettingswidget.h"
#include "schedulewidget.h"
#include <QSpacerItem>
#include <QFileDialog>

DeviceView::DeviceView(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DeviceView)
{
    ui->setupUi(this);
    _dev = NULL;
    initialized = false;
    servosHidden = true;
}

DeviceView::~DeviceView()
{
    clearLayout(ui->ltServos);
    clearLayout(ui->ltServoSettings);
    clearLayout(ui->ltSchedule);
    delete ui;
}

void DeviceView::setDevice(Device *dev)
{
    _dev = dev;
    connect(_dev,SIGNAL(stateChanged(Device::State)),this,SLOT(deviceStateChanged(Device::State)));
    connect(_dev,SIGNAL(busyChange(bool)),this,SLOT(setBusy(bool)));
    connect(_dev,SIGNAL(updated()),this,SLOT(reinit()));
}

void DeviceView::setTabI(int ti)
{
    _tabI = ti;
}

void DeviceView::deviceStateChanged(Device::State state)
{
    if (state == Device::State::Offline)
    {
    }
    if (state == Device::State::Initializing)
    {
    }
    if (state == Device::State::Ready)
    {
        if (!initialized)
        {
            clearLayout(ui->ltServos);
            clearLayout(ui->ltServoSettings);
            clearLayout(ui->ltSchedule);
            //servos widgets
            int row = 0;
            int col = 0;
            for(int i=0;i<_dev->config().servos.length();i++)
            {
                if (!_dev->config().servos.at(i).enabled)
                    continue;
                ServoWidget * swgt = new ServoWidget(this);
                ui->ltServos->addWidget(swgt,row,col);
                swgt->init(_dev,i);
                col++;
                if (col==2)
                {
                    col = 0;
                    row++;
                }
            }
            QSpacerItem * spacer = new QSpacerItem(1,1,QSizePolicy::Expanding,QSizePolicy::Expanding);
            ui->ltServos->addItem(spacer,row+1,0);


            //settings
            ui->ssName->setText(_dev->config().name);
            ui->ssWifiEssid->setText(_dev->config().wifiEssid);
            ui->ssWifiPassword->setText(_dev->config().wifiPassword);
            if (_dev->config().wifiIsAP)
                ui->ssModeAP->setChecked(true);
            else
                ui->ssModeLN->setChecked(true);
            ui->ssMasterIP->setText(_dev->config().masterIP);
            ui->ssMastrPort->setValue(_dev->config().masterPort);
            ui->ssMasterLock->setChecked(_dev->config().masterLock);

            //servos settings
            row = 0;
            col = 0;
            for(int i=0;i<_dev->config().servos.length();i++)
            {
                ServoSettingsWidget* swgt = new ServoSettingsWidget(this);
                swgt->init(_dev,i);
                ui->ltServoSettings->addWidget(swgt,row,col);
                col++;
                if (col==2)
                {
                    col = 0;
                    row++;
                }
            }
            spacer = new QSpacerItem(1,1,QSizePolicy::Expanding,QSizePolicy::Expanding);
            ui->ltServoSettings->addItem(spacer,row+1,0);

            //jobs
            foreach(Device::Job job, _dev->jobs())
            {
                ScheduleWidget * wgt = new ScheduleWidget(this);
                wgt->init(_dev,job);
                ui->ltSchedule->addWidget(wgt);
                connect(wgt,SIGNAL(dropMe()),this,SLOT(dropJob()));
            }
            spacer = new QSpacerItem(1,1,QSizePolicy::Expanding,QSizePolicy::Expanding);
            ui->ltSchedule->addItem(spacer);

            initialized = true;
        }
    }
    emit updateTabTitle(_tabI,_dev->title());
}

void DeviceView::reinit()
{
    emit updateTabTitle(_tabI,_dev->title());
    initialized = false;
}


void DeviceView::on_pushButton_clicked()
{
    for(int i=0;i<ui->ltServoSettings->count();i++)
    {
        QLayoutItem * li = ui->ltServoSettings->itemAt(i);
        ServoSettingsWidget * wgt = qobject_cast<ServoSettingsWidget*>(li->widget());
        if (wgt)
        {
            qDebug()<<"wgt!";
            wgt->save();
        }
    }
    _dev->saveServosConfig();
}

void DeviceView::setBusy(bool busy)
{
    ui->pushButton->setEnabled(!busy);
}


void DeviceView::on_pbSaveMainSettings_clicked()
{
    Device::Config cfg = _dev->config();
    cfg.name = ui->ssName->text();
    cfg.wifiEssid = ui->ssWifiEssid->text();
    cfg.wifiPassword = ui->ssWifiPassword->text();
    cfg.wifiIsAP = ui->ssModeAP->isChecked();
    cfg.masterIP = ui->ssMasterIP->text();
    cfg.masterPort = ui->ssMastrPort->value();
    cfg.masterLock = ui->ssMasterLock->isChecked();

    _dev->saveMainConfig(cfg);
}

void DeviceView::clearLayout(QLayout *lt)
{
    QLayoutItem *item;
    while((item = lt->takeAt(0))) {
        if (item->layout()) {
            clearLayout(item->layout());
            delete item->layout();
        }
        if (item->widget()) {
           delete item->widget();
        }
        delete item;
    }
}

void DeviceView::on_pbAddSchedule_clicked()
{
    ScheduleWidget * wgt = new ScheduleWidget(this);
    Device::Job job;
    job.servo = 1;
    job.name = "";
    job.cron = "* * * * * *";
    job.angle = 0;
    job.time = 0;
    wgt->init(_dev,job);
    connect(wgt,SIGNAL(dropMe()),this,SLOT(dropJob()));
    ui->ltSchedule->insertWidget(ui->ltSchedule->count()-1,wgt);
}

void DeviceView::on_pbSaveSchedule_clicked()
{
    QList<Device::Job> lst;
    for(int i=0;i<ui->ltSchedule->count();i++)
    {
        QLayoutItem *li = ui->ltSchedule->itemAt(i);
        ScheduleWidget *w = qobject_cast<ScheduleWidget*>(li->widget());
        if (!w)
            continue;
        Device::Job j = w->getJob();
        lst.append(j);
    }
    _dev->saveDevIDJobs(lst);
}

void DeviceView::dropJob()
{
    ScheduleWidget *w = qobject_cast<ScheduleWidget*>(sender());
    ui->ltSchedule->removeWidget(w);
    w->deleteLater();
}

void DeviceView::on_pbSStoFile_clicked()
{
    QString fname = QFileDialog::getSaveFileName(this,tr("Save jobs to file"),"",".json");
    qDebug() << "save to " << fname;
    QList<Device::Job> lst;
    for(int i=0;i<ui->ltSchedule->count();i++)
    {
        QLayoutItem *li = ui->ltSchedule->itemAt(i);
        ScheduleWidget *w = qobject_cast<ScheduleWidget*>(li->widget());
        if (!w)
            continue;
        Device::Job j = w->getJob();
        lst.append(j);
    }
    _dev->saveSchedules(lst,fname);

}
