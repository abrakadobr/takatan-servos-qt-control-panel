#include "servosettingswidget.h"
#include "ui_servosettingswidget.h"

ServoSettingsWidget::ServoSettingsWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ServoSettingsWidget)
{
    ui->setupUi(this);
}

ServoSettingsWidget::~ServoSettingsWidget()
{
    delete ui;
}

void ServoSettingsWidget::init(Device *dev, int si)
{
    _dev = dev;
    _si = si;
    connect(_dev,SIGNAL(busyChange(bool)),this,SLOT(setBusy(bool)));
//    if (_si>=_dev->config().servos.length())
//        return;
    Device::ServoConfig cfg = _dev->config().servos.at(_si);
    ui->groupBox->setTitle("Servo "+QString::number(_si+1));
    ui->groupBox->setChecked(cfg.enabled);
    ui->sbAngle->setValue(cfg.clickAngle);
    ui->sbTime->setValue(cfg.clickTime);
    ui->sbMin->setValue(cfg.min);
    ui->sbMax->setValue(cfg.max);
}

void ServoSettingsWidget::save()
{
    if (_si>=_dev->config().servos.length())
        return;
    Device::ServoConfig cfg = _dev->config().servos.at(_si);
    cfg.enabled = ui->groupBox->isChecked();
    cfg.clickAngle = ui->sbAngle->value();
    cfg.clickTime = ui->sbTime->value();
    cfg.min = ui->sbMin->value();
    cfg.max = ui->sbMax->value();
    _dev->setServoConfig(_si,cfg);
}

void ServoSettingsWidget::setBusy(bool busy)
{
    if (_si>=_dev->config().servos.length())
        return;
    Device::ServoConfig cfg = _dev->config().servos.at(_si);
    if (!cfg.enabled)
        return;
    ui->sbAngle->setEnabled(!busy);
    ui->sbTime->setEnabled(!busy);
    ui->sbMin->setEnabled(!busy);
    ui->sbMax->setEnabled(!busy);
}
