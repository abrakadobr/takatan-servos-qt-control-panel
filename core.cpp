#include "core.h"

#include <QSettings>
#include <QDir>

Core::Core(QObject *parent) : QObject(parent)
{

    if (!QDir("./dev").exists())
        QDir().mkdir("./dev");
//    loadKnownServos();
    httpServer = new HTTPServer(this);
    connect(httpServer,SIGNAL(ping(QString,QString)),this,SLOT(pingFrom(QString,QString)));
    restartHttpServer();
}

Core::~Core()
{
    httpServer->close();
    httpServer->deleteLater();
    QStringList ids = devices.keys();
    foreach (QString id, ids) {
        devices.value(id)->deleteLater();
    }
}

Device *Core::deviceByID(QString did)
{
    return devices.value(did,NULL);
}

Device *Core::registerDevice(QString id, QString ip)
{
    Device * dev = new Device(this);
    connect(dev,SIGNAL(stateChanged(Device::State)),this,SLOT(deviceStateUpdated(Device::State)));
    connect(dev,SIGNAL(startJob(Device::Job)),this,SLOT(deviceStartedJob(Device::Job)));
    devices.insert(id,dev);
    dev->initByIDIP(id,ip);
    emit newDevice(dev);
    return dev;
}

Device *Core::loadDevice(QString id)
{
    Device * dev = new Device(this);
    connect(dev,SIGNAL(stateChanged(Device::State)),this,SLOT(deviceStateUpdated(Device::State)));
    connect(dev,SIGNAL(startJob(Device::Job)),this,SLOT(deviceStartedJob(Device::Job)));

    qDebug() << "loading servo " << id;
    dev->initFromFile(id);

    devices.insert(id,dev);
    emit newDevice(dev);
    return dev;
}

void Core::settingsUpdated()
{
    restartHttpServer();
}

void Core::restartHttpServer()
{
    if (httpServer->isListening())
        httpServer->close();
    QSettings s;
    httpServer->listen(QHostAddress::AnyIPv4,s.value("httpserver/port").toInt());
}

void Core::pingFrom(QString ip, QString devid)
{
    Device * dev = deviceByID(devid);
    if (!dev)
        dev = registerDevice(devid,ip);
    dev->gotPing(ip);
}

void Core::deviceStateUpdated(Device::State state)
{
    QSettings s;
    if (!s.value("notify/state",false).toBool())
        return;
    Device * d = qobject_cast<Device*>(sender());
    if (!d)
        return;
    QString st = "Device offline";
    if (state==Device::State::Initializing)
        st = "Initializing device";
    if (state==Device::State::Ready)
        st = "Device online";
    emit notifyDeviceState(st,d->title());
}

void Core::deviceStartedJob(Device::Job job)
{
    QSettings s;
    if (!s.value("notify/job",false).toBool())
        return;
    Device * d = qobject_cast<Device*>(sender());
    if (!d)
        return;
    QString inf =  job.name+" ["+QString::number(job.servo)+"> "+QString::number(job.angle)+"/"+QString::number(job.time)+"]";
    QString ttl = "Job "+d->config().name;
    emit notifyDeviceState(ttl,inf);
}

void Core::loadKnownServos()
{
    QDir d("./dev");
    if (!d.exists()||d.isEmpty())
        return;
    foreach(QString e,d.entryList())
    {
        if (!e.endsWith(".cfg.json"))
            continue;
        QString did = e.left(6);
        Device * dev = deviceByID(did);
        if (dev)
            continue;
        loadDevice(did);
//        qDebug() << e << " " << did;
    }
}
