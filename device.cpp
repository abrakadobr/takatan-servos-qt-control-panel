#include "device.h"
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QUrlQuery>
#include <QJsonDocument>
#include <QFile>

#include <QDebug>

Device::Device(QObject *parent) : QObject(parent)
{
    _state = Offline;
    connect(&t1s,SIGNAL(timeout()),this,SLOT(on1secTimer()));
    t1s.setInterval(1000);
    t1s.setSingleShot(false);
    t1s.start();
}

void Device::initByIDIP(QString id,QString ip)
{
    _cfg.devID = id;
    _ip = ip;
    if (!t1s.isActive())
        t1s.start();
    qDebug() << "init by id ip" << id << " - " << ip;
    lastPing = QDateTime::currentDateTime();
    lastSPing = QDateTime::currentSecsSinceEpoch();
    update();
}

void Device::initFromFile(QString id)
{
    load(id);
}

QString Device::title()
{
    QString icon = "[offline] ";
    if (state()==Initializing)
        icon = "[init] ";
    if (state()==Ready)
        icon = "[online] ";
    return icon + _cfg.name+" ["+_cfg.devID+"] <" +_ip+">";
}

QString Device::IP()
{
    return _ip;
}

Device::State Device::state()
{
    return _state;
}

Device::Config Device::config()
{
    return _cfg;
}

QList<Device::Job> Device::jobs()
{
    return _jobs;
}

void Device::saveDevIDJobs(QList<Job> jobslist)
{
    _jobs = jobslist;
    saveSchedules(jobslist,"./dev/"+_cfg.devID+".jobs.json");
}

void Device::save()
{
    QString filePath = "./dev/"+_cfg.devID+".cfg.json";
    QFile f(filePath);
    if (!f.open(QIODevice::WriteOnly))
        return;

    QVariantMap data;
    QVariantMap wifi;
    QVariantMap master;
    QVariantList servos;
//    QVariantList jobs;


    wifi.insert("essid",_cfg.wifiEssid);
    wifi.insert("password",_cfg.wifiPassword);
    wifi.insert("isAP",_cfg.wifiIsAP);

    master.insert("ip",_cfg.masterIP);
    master.insert("port",_cfg.masterPort);
    master.insert("lock",_cfg.masterLock);

    foreach(Device::ServoConfig scfg, _cfg.servos)
    {
        QVariantMap mscfg;
        mscfg.insert("on",scfg.enabled);
        mscfg.insert("angle",scfg.clickAngle);
        mscfg.insert("time",scfg.clickTime);
        mscfg.insert("min",scfg.min);
        mscfg.insert("max",scfg.max);
        servos.append(mscfg);
    }

    /*
    foreach(Device::Job j, _jobs)
    {
        QVariantMap jm;
        jm.insert("name",j.name);
        jm.insert("servo",j.servo);
        jm.insert("cron",j.cron);
        jm.insert("angle",j.angle);
        jm.insert("time",j.time);
        jobs.append(jm);
    }
    */

    data.insert("deviceID",_cfg.devID);
    data.insert("name",_cfg.name);
    data.insert("wifi",wifi);
    data.insert("master",master);
    data.insert("servos",servos);
//    data.insert("jobs",jobs);

    QJsonDocument jdoc = QJsonDocument::fromVariant(data);
    QTextStream ts(&f);
    ts << jdoc.toJson();
    f.close();
}

void Device::load(QString deviceId)
{
    _cfg.devID = deviceId;
    QFile f("./dev/"+deviceId+".cfg.json");
    if (!f.exists())
        return;
    if (!f.open(QIODevice::ReadOnly))
        return;
    QJsonDocument jdoc(QJsonDocument::fromJson(f.readAll()));
    if (jdoc.isNull())
    {
        qDebug() << "null json";
        return;
    }
    QVariantMap data = jdoc.toVariant().toMap();
//    qDebug() << "file? " << data;

    QVariantMap wifi = data.value("wifi").toMap();
    QVariantMap master = data.value("master").toMap();
    QVariantList servos = data.value("servos").toList();

    _cfg.name = data.value("name","").toString();

    _cfg.wifiEssid = wifi.value("essid","").toString();
    _cfg.wifiPassword = wifi.value("password","").toString();
    _cfg.wifiIsAP = wifi.value("isAP",false).toBool();

    _cfg.masterIP = master.value("ip","").toString();
    _cfg.masterPort = master.value("port",0).toInt();
    _cfg.masterLock = master.value("lock",false).toBool();

    _cfg.servos.clear();
    foreach(QVariant v,servos)
    {
        QVariantMap vm = v.toMap();
        Device::ServoConfig sc;
        sc.enabled = vm.value("on",false).toBool();
        sc.clickAngle = vm.value("angle",0).toInt();
        sc.clickTime = vm.value("time",0).toInt();
        sc.min = vm.value("min",-115).toInt();
        sc.max = vm.value("max",115).toInt();
        _cfg.servos.append(sc);
    }
    loadSchedules("./dev/"+_cfg.devID+".jobs.json");
    _state = Offline;
    emit stateChanged(_state);
    emit busyChange(true);
    emit updated();
}

void Device::loadSchedules(QString filePath)
{
    QFile f(filePath);
    if (!f.exists())
        return;
    if (!f.open(QIODevice::ReadOnly))
        return;
    QJsonDocument jdoc(QJsonDocument::fromJson(f.readAll()));
    if (jdoc.isNull())
    {
        qDebug() << "null json";
        return;
    }

    _jobs.clear();
    QVariantList lst = jdoc.toVariant().toList();
    if (!lst.length())
        return;
    foreach(QVariant v, lst)
    {
        QVariantMap mp = v.toMap();
        Device::Job j;
        j.angle = mp.value("angle").toInt();
        j.cron = mp.value("cron").toString();
        j.name = mp.value("name").toString();
        j.servo = mp.value("servo").toInt();
        j.time = mp.value("time").toInt();
        _jobs.append(j);
    }
}

void Device::saveSchedules(QList<Job> jobslist, QString filePath)
{
    QFile f(filePath);
    if (!f.open(QIODevice::WriteOnly))
        return;
    QVariantList lst;
    foreach (Device::Job j, jobslist)
    {
        QVariantMap jm;
        jm.insert("angle",j.angle);
        jm.insert("cron",j.cron);
        jm.insert("name",j.name);
        jm.insert("servo",j.servo);
        jm.insert("time",j.time);
        lst.append(jm);
    }
    QJsonDocument jdoc = QJsonDocument::fromVariant(lst);
    QTextStream ts(&f);
    ts << jdoc.toJson();
    f.close();
}

void Device::gotPing(QString ip)
{
    _ip = ip;
    lastSPing = QDateTime::currentSecsSinceEpoch();
    if (_state==Offline)
        update();
//    qDebug() << "ping: " << _cfg.name << " [" <<_cfg.devID << "]";
}

void Device::update()
{
    emit busyChange(true);
    _state = Initializing;
    emit stateChanged(_state);
    QString url = "http://"+_ip+"/cfg";
    qDebug() << "posting " << url;
    QNetworkReply * rpl = qna.get(QNetworkRequest(url));
    connect(rpl,SIGNAL(finished()),this,SLOT(replyCfgFinished()));
}

void Device::saveMainConfig(Device::Config cfg)
{
    if (_state!=Ready)
        return;
    emit busyChange(true);
    QString url = "http://"+_ip+"/cfgmain";
    QUrlQuery data;
    data.addQueryItem("dn",cfg.name);
    data.addQueryItem("we",cfg.wifiEssid);
    data.addQueryItem("wp",cfg.wifiPassword);
    data.addQueryItem("wm",cfg.wifiIsAP?"1":"0");
    data.addQueryItem("mi",cfg.masterIP);
    data.addQueryItem("mp",QString::number(cfg.masterPort));
    data.addQueryItem("ml",cfg.masterLock?"1":"0");

    QNetworkRequest req(url);
    req.setHeader(QNetworkRequest::ContentTypeHeader,"application/x-www-form-urlencoded");
    QString pd = data.toString();
    qDebug() << "posing " <<url<< " data: "<< pd;
    QNetworkReply * rpl = qna.post(req,pd.toLocal8Bit());
    connect(rpl,SIGNAL(finished()),this,SLOT(replyMainConfigFinished()));
}


void Device::saveServosConfig()
{
    if (_state!=Ready)
        return;
    emit busyChange(true);
    QString url = "http://"+_ip+"/cfgs";
    QUrlQuery data;
    int i=0;
    foreach(Device::ServoConfig scfg, _cfg.servos)
    {
        data.addQueryItem("o"+QString::number(i),scfg.enabled?"1":"0");
        data.addQueryItem("mi"+QString::number(i),QString::number(scfg.min));
        data.addQueryItem("mx"+QString::number(i),QString::number(scfg.max));
        data.addQueryItem("a"+QString::number(i),QString::number(scfg.clickAngle));
        data.addQueryItem("t"+QString::number(i),QString::number(scfg.clickTime));
        i++;
    }
    QNetworkRequest req(url);
    req.setHeader(QNetworkRequest::ContentTypeHeader,"application/x-www-form-urlencoded");
    QString pd = data.toString();
    qDebug() << "posting " <<url<< " data: "<< pd;
    QNetworkReply * rpl = qna.post(req,pd.toLocal8Bit());
    connect(rpl,SIGNAL(finished()),this,SLOT(replyServoCfgFinished()));
//    emit stateChanged(_state);
}

void Device::setServoConfig(int si, Device::ServoConfig cfg)
{
    _cfg.servos.replace(si,cfg);
}

void Device::servoGoTo(int sid, int angle, int time)
{
    if (_state!=Ready)
        return;
    emit busyChange(true);
//    _state = Initializing;
    QString url = "http://"+_ip+"/goto";
    QUrlQuery data;
    data.addQueryItem("a"+QString::number(sid),QString::number(angle));
    if (time > 0)
        data.addQueryItem("t"+QString::number(sid),QString::number(time));
    QNetworkRequest req(url);
    req.setHeader(QNetworkRequest::ContentTypeHeader,"application/x-www-form-urlencoded");
    QString pd = data.toString();
    qDebug() << "posting " <<url<< " data: "<< pd;
    QNetworkReply * rpl = qna.post(req,pd.toLocal8Bit());
    connect(rpl,SIGNAL(finished()),this,SLOT(replyGoToFinished()));
}

void Device::replyCfgFinished()
{
    QNetworkReply * rpl = qobject_cast<QNetworkReply*>(sender());
    QByteArray bdata = rpl->readAll();
//    QString jsons = QString(bdata);
    QJsonDocument jdoc(QJsonDocument::fromJson(bdata));
    if (jdoc.isNull())
    {
        qDebug() << "null json";
        return;
    }
    QVariantMap inf = jdoc.toVariant().toMap();
    QVariantMap wifi = inf.value("wifi").toMap();
    QVariantMap master = inf.value("master").toMap();
    _cfg.devID = inf.value("devID").toString();
    _cfg.name = inf.value("name").toString();

    _cfg.wifiEssid = wifi.value("essid").toString();
    _cfg.wifiPassword = wifi.value("pass").toString();
    _cfg.wifiIsAP = wifi.value("isAP").toBool();

    _cfg.masterIP = master.value("ip").toString();
    _cfg.masterPort = master.value("port").toInt();
    _cfg.masterLock = master.value("lock").toBool();

    QVariantList servos = inf.value("servos").toList();
    int si = 0;
    _cfg.servos.clear();
    foreach(QVariant s, servos)
    {
        QVariantMap srv = s.toMap();
        Device::ServoConfig scfg;
        scfg.num = si;
        scfg.enabled = srv.value("on").toBool();
        scfg.min= srv.value("mi").toInt();
        scfg.max= srv.value("mx").toInt();
        scfg.clickAngle = srv.value("ca").toInt();
        scfg.clickTime = srv.value("ct").toInt();
        _cfg.servos.append(scfg);
        si++;
    }

//    qDebug() << "cfg reply: " << inf << "\n\n" \
             << _cfg.name << "/" << _cfg.wifiEssid << "[" << _cfg.wifiPassword << "|" << _cfg.wifiIsAP <<"]\n" \
             << _cfg.masterIP << ":" << _cfg.masterPort << " |"<<_cfg.masterLock;
//    foreach(Device::ServoConfig scfg,_cfg.servos)
//    {
//        qDebug() << "servo[" << scfg.num << "] ON:" << scfg.enabled << " MIN:" << scfg.min << " MAX:" << scfg.max << " CA:"<<scfg.clickAngle;
//    }
    rpl->close();
    rpl->deleteLater();
    loadSchedules("./dev/"+_cfg.devID+".jobs.json");
    save();
    _state = Ready;
    emit stateChanged(_state);
    emit busyChange(false);
    emit updated();
}

void Device::replyServoCfgFinished()
{
    QNetworkReply * rpl = qobject_cast<QNetworkReply*>(sender());
    QByteArray data = rpl->readAll();
    save();
//    qDebug() << "post /cfgs " << QString(data);
    rpl->close();
    rpl->deleteLater();
    update();

}

void Device::replyGoToFinished()
{
    QNetworkReply * rpl = qobject_cast<QNetworkReply*>(sender());
//    QByteArray data = rpl->readAll();
//    qDebug() << "post /cfgs " << QString(data);
    rpl->close();
    rpl->deleteLater();
    emit busyChange(false);
}

void Device::replyMainConfigFinished()
{
    QNetworkReply * rpl = qobject_cast<QNetworkReply*>(sender());
//    QByteArray data = rpl->readAll();
//    qDebug() << "post /cfgs " << QString(data);
    rpl->close();
    rpl->deleteLater();
    update();
}

void Device::on1secTimer()
{
    if (QDateTime::currentSecsSinceEpoch()-lastSPing>6&&_state!=Offline)
    {
        qDebug() << "OFFLINE!";
        _state = Offline;
        emit stateChanged(_state);
        emit busyChange(true);
    } else {
        emit busyChange(false);
    }
    foreach(Device::Job j, _jobs)
    {
        if (checkJob(j))
            doJob(j);
    }
}

bool Device::checkJob(Device::Job j)
{
    if (_state!=Ready)
        return false;
    if (!j.cron.length())
        return false;
    QStringList crons = j.cron.split(" ");
    if (crons.length()!= 6)
        return false;
    QDateTime now = QDateTime::currentDateTime();
    if (!checkCronNumber(now.time().second(),crons.at(0)))
        return false;
    if (!checkCronNumber(now.time().minute(),crons.at(1)))
        return false;
    if (!checkCronNumber(now.time().hour(),crons.at(2)))
        return false;
    if (!checkCronNumber(now.date().day(),crons.at(3)))
        return false;
    if (!checkCronNumber(now.date().month(),crons.at(4)))
        return false;
    int dow = now.date().dayOfWeek();
    if (dow==7)
        dow = 0;
    if (!checkCronNumber(dow,crons.at(5)))
        return false;
    return true;
}

bool Device::checkCronNumber(int n, QString cron)
{
    if (cron=="*")
        return true;
//    QStringLi
    bool ret = false;
    foreach(QString b,cron.split(","))
    {
        if (ret)
            continue;
        if (!b.contains("-")&&!b.contains("/"))
        {
            //should be single number
            ret = b.toInt() == n;
            continue;
        }
        if (b.contains("-"))
        {
            //diapason
            QStringList vals = b.split("-");
            if (vals.length()==2)
            {
                int from = vals.at(0).toInt();
                int to = vals.at(1).toInt();
                if (n>=from&&n<=to)
                    ret = true;
            }
        } else {
            //division
            QStringList vals = b.split("/");
            if (vals.length()==2&&vals.at(0)=="*")
            {
                int div = vals.at(1).toInt();
                if (div>0)
                {
                    if (n%div==0)
                        ret = true;
                }
            }
        }
    }
    return ret;
}

void Device::doJob(Device::Job j)
{
    qDebug() << "JOB " << j.name;
    emit startJob(j);
    servoGoTo(j.servo-1,j.angle,j.time);
}
