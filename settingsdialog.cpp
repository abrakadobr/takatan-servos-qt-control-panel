#include "settingsdialog.h"
#include "ui_settingsdialog.h"

#include <QSettings>

SettingsDialog::SettingsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SettingsDialog)
{
    ui->setupUi(this);
    QSettings s;
    ui->spinBox->setValue(s.value("httpserver/port",18881).toInt());
    ui->cbNotifyState->setChecked(s.value("notify/state",false).toBool());
    ui->cbNotifyJob->setChecked(s.value("notify/job",false).toBool());
}

SettingsDialog::~SettingsDialog()
{
    delete ui;
}

void SettingsDialog::on_buttonBox_accepted()
{
    QSettings s;
    s.setValue("httpserver/port",ui->spinBox->value());
    s.setValue("notify/state",ui->cbNotifyState->isChecked());
    s.setValue("notify/job",ui->cbNotifyJob->isChecked());
    emit done(true);
}

void SettingsDialog::on_buttonBox_rejected()
{
    emit done(false);
}
